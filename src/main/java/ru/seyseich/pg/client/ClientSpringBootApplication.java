package ru.seyseich.pg.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import ru.seyseich.pg.audit.lib.requestcontext.RequestContextConfiguration;

@SpringBootApplication
@Import(RequestContextConfiguration.class)
public class ClientSpringBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClientSpringBootApplication.class, args);
    }
}
