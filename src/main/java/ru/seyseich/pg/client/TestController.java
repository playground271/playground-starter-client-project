package ru.seyseich.pg.client;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.seyseich.pg.audit.lib.AuditMessageType;
import ru.seyseich.pg.audit.lib.aspect.AuditEvent;

@RestController
public class TestController {

    @AuditEvent(
            type = AuditMessageType.CREATE,
            action = "Вызван тестовый эндпоинт"
    )
    @AuditEvent(
            type = AuditMessageType.UPDATE,
            action = "А тут еще одно событие!"
    )
    @GetMapping("/test")
    public String test() {
        return "Success!";
    }
}
